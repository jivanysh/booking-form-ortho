# BookingForm plugin for Craft CMS 3.x

Test

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /booking-form

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for BookingForm.

## BookingForm Overview

-Insert text here-

## Configuring BookingForm

-Insert text here-

## Using BookingForm

-Insert text here-

## BookingForm Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Jivanysh](github.com/omfg5716)
