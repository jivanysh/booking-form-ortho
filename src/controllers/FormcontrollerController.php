<?php
/**
 * BookingForm plugin for Craft CMS 3.x
 *
 * Test
 *
 * @link      github.com/omfg5716
 * @copyright Copyright (c) 2019 Jivanysh
 */

namespace jivanysh\bookingform\controllers;

use jivanysh\bookingform\BookingForm;

use Craft;
use craft\web\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Formcontroller Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Jivanysh
 * @package   BookingForm
 * @since     1
 */
class FormcontrollerController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something', 'second-step'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/booking-form/formcontroller
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->requirePostRequest();


        $result = 'Welcome to the FormcontrollerController actionIndex() method';
        // var_dump($_POST);

        $client = new \GuzzleHttp\Client(
            array( 
                   'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                   'verify' => false
                 )
             );
        
		$clientResponse = $client->post('https://2c9d8512.ngrok.io/api/v1/lead', [
			RequestOptions::JSON => [ 
				'name' => $_POST['firstName'],
				'email' => $_POST['email'],
				'phone' => $_POST['phoneNumber'],
                'organization' => 'Burns Ortho',
			]
		]);

        return $clientResponse->getBody();
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/booking-form/formcontroller/do-something
     *
     * @return mixed
     */
    public function actionSecond() {
        $this->requirePostRequest();

        $client = new \GuzzleHttp\Client(
            array( 
                'curl'   => array( CURLOPT_SSL_VERIFYPEER => false ),
                'verify' => false
            )
        );


        $client->setDefaultOption('verify', false);

		$clientResponse = $client->put('https://2c9d8512.ngrok.io/api/v1/lead', [
			RequestOptions::JSON => [ 
                'leadId' => $_POST['leadId'],
                'customFields' => [
                    'treatment' => $_POST['treatment'],
                    'location' => $_POST['location'],
                    'reasonForAppointment' => $_POST['reasonForAppointment'],    
                ],
				'organization' => 'Burns Ortho',
			]
		]);

        return json_encode(var_dump($_POST));
    }

}
