<?php
/**
 * BookingForm plugin for Craft CMS 3.x
 *
 * Test
 *
 * @link      github.com/omfg5716
 * @copyright Copyright (c) 2019 Jivanysh
 */

/**
 * BookingForm en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('booking-form', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Jivanysh
 * @package   BookingForm
 * @since     1
 */
return [
    'BookingForm plugin loaded' => 'BookingForm plugin loaded',
];
